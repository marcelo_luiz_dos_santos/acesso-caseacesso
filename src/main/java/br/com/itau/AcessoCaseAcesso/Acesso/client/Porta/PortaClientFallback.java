package br.com.itau.AcessoCaseAcesso.Acesso.client.Porta;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class PortaClientFallback implements PortaClient {

    @Override
    public Porta buscarPorId(int id) {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço PORTA indisponível");
    }
}
