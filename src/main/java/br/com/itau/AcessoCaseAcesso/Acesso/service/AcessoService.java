package br.com.itau.AcessoCaseAcesso.Acesso.service;

import br.com.itau.AcessoCaseAcesso.Acesso.DTO.AcessoKafka;
import br.com.itau.AcessoCaseAcesso.Acesso.client.Cliente.Cliente;
import br.com.itau.AcessoCaseAcesso.Acesso.client.Cliente.ClienteClient;
import br.com.itau.AcessoCaseAcesso.Acesso.client.Porta.Porta;
import br.com.itau.AcessoCaseAcesso.Acesso.client.Porta.PortaClient;
import br.com.itau.AcessoCaseAcesso.Acesso.model.Acesso;
import br.com.itau.AcessoCaseAcesso.Acesso.repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private KafkaTemplate<String, AcessoKafka> producer;

    public Acesso criarAcesso(Acesso acesso){
        Cliente cliente = clienteClient.buscarPorId(acesso.getClienteId());

        Porta porta = portaClient.buscarPorId((acesso.getPortaId()));

        Acesso acessoObjeto = acessoRepository.save(acesso);
        return acessoObjeto;
    }

    public Optional<Acesso> buscarPorId (int clienteId, int portaId){
        Optional<Acesso> optionalAcesso = acessoRepository.findByClienteIdAndPortaId(clienteId, portaId);
        if (optionalAcesso.isPresent()){
            return optionalAcesso;
        }
        throw new RuntimeException("Acesso não encontrado");
    }

    @Transactional
    public void deletarAcesso (int clienteId, int portaId){
        Optional<Acesso> optionalAcesso = acessoRepository.findByClienteIdAndPortaId(clienteId, portaId);
        if (optionalAcesso.isPresent()){
            acessoRepository.removeAllByClienteIdAndPortaId(clienteId, portaId);
        }else{
            throw new RuntimeException("O Acesso não foi encontrado");
        }
    }

    public void enviarAoKafka(AcessoKafka acesso) {
//        for (int i = 0; i < 40; i++) {
//            producer.send("spec4-biblioteca", i, "1", livro);
//        }
        producer.send("spec4-marcelo-luiz-1", acesso);
    }
}
