package br.com.itau.AcessoCaseAcesso.Acesso.DTO;

public class AcessoKafka {

    private int portaId;
    private int clienteId;
    private Boolean acessoLiberado;

    public AcessoKafka() {
    }

    public int getPortaId() {
        return portaId;
    }

    public void setPortaId(int portaId) {
        this.portaId = portaId;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public Boolean getAcessoLiberado() {
        return acessoLiberado;
    }

    public void setAcessoLiberado(Boolean acessoLiberado) {
        this.acessoLiberado = acessoLiberado;
    }
}
