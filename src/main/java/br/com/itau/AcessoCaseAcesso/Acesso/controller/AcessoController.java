package br.com.itau.AcessoCaseAcesso.Acesso.controller;

import br.com.itau.AcessoCaseAcesso.Acesso.DTO.AcessoKafka;
import br.com.itau.AcessoCaseAcesso.Acesso.model.Acesso;
import br.com.itau.AcessoCaseAcesso.Acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Acesso criarAcesso (@RequestBody @Valid Acesso acesso){
        Acesso acessoObjeto = acessoService.criarAcesso(acesso);
        return acessoObjeto;
    }

    @GetMapping("/{clienteId}/{portaId}")
    public Optional<Acesso> buscarPorId (@PathVariable(name = "clienteId") int clienteId,
                                         @PathVariable(name = "portaId") int portaId){
        try{
            Optional<Acesso> acesso = acessoService.buscarPorId(clienteId, portaId);
            AcessoKafka acessoKafka = new AcessoKafka();
            acessoKafka.setClienteId(clienteId);
            acessoKafka.setPortaId(portaId);
            acessoKafka.setAcessoLiberado(true);
            acessoService.enviarAoKafka(acessoKafka);
            return acesso;
        }catch (RuntimeException exception){
            AcessoKafka acessoKafka = new AcessoKafka();
            acessoKafka.setClienteId(clienteId);
            acessoKafka.setPortaId(portaId);
            acessoKafka.setAcessoLiberado(false);
            acessoService.enviarAoKafka(acessoKafka);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    @DeleteMapping("/{clienteId}/{portaId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarAcesso (@PathVariable(name = "clienteId") int clienteId,
                               @PathVariable(name = "portaId") int portaId){
        try{
            acessoService.deletarAcesso(clienteId, portaId);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

}
