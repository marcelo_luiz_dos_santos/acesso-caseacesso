package br.com.itau.AcessoCaseAcesso.Acesso.client.Cliente;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cliente não encontrado")
public class ClienteNotFoundException extends RuntimeException{

}
