package br.com.itau.AcessoCaseAcesso.Acesso.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Acesso {
    @Id
    @NotNull
    private int portaId;
    @NotNull
    private int clienteId;

    public Acesso() {
    }

    public int getPortaId() {
        return portaId;
    }

    public void setPortaId(int portaId) {
        this.portaId = portaId;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
