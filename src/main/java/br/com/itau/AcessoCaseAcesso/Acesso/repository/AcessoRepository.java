package br.com.itau.AcessoCaseAcesso.Acesso.repository;

import br.com.itau.AcessoCaseAcesso.Acesso.model.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository <Acesso, Integer> {
    Optional<Acesso> findByClienteIdAndPortaId(int clienteId, int portaId);
    void removeAllByClienteIdAndPortaId(int clienteId, int portaId);
}
