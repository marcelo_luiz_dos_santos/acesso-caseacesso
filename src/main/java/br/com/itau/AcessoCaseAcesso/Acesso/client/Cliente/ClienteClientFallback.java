package br.com.itau.AcessoCaseAcesso.Acesso.client.Cliente;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public Cliente buscarPorId(int id) {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço CLIENTE indisponível");
    }
}
