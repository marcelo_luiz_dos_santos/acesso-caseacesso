package br.com.itau.AcessoCaseAcesso.Acesso.client.Porta;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Porta não encontrada")
public class PortaNotFoundException extends RuntimeException{

}
