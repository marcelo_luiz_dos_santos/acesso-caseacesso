package br.com.itau.AcessoCaseAcesso.Acesso.client.Porta;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "PORTA", configuration = PortaClientConfiguration.class)
public interface PortaClient {
    @GetMapping("/porta/{id}")
    Porta buscarPorId(@PathVariable int id);
}
