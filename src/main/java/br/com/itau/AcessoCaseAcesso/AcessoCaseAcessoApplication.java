package br.com.itau.AcessoCaseAcesso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class AcessoCaseAcessoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcessoCaseAcessoApplication.class, args);
	}

}
